package shahen.file;

import java.io.*;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class FileHelper {
    public final static Logger log = Logger.getLogger("shahen");
    /** A file extension, including the period, 
     * that will be used to search for files when in BULK mode 
     */
    public String fileExt = ".mohawkT";
    public ArrayList<File> files = new ArrayList<File>();
    public boolean bulk = false;

    public String lastError = null;
    public StringBuilder errorLog = new StringBuilder();

    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////

    public FileHelper() {}
    public FileHelper(String fileExtension) {
        fileExt = fileExtension;
    }

    /**
     * Returns the SHA256 Hash for a file
     * @param file the file to be hashed
     * @return a 256 character string (make sure to save space for the NULL zero, 257 characters).
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static String sha256(File file) throws IOException, NoSuchAlgorithmException {
        return hashFile(file, "SHA-256", null);
    }
    public static String sha256(File file, String suffixStr) throws IOException, NoSuchAlgorithmException {
        return hashFile(file, "SHA-256", suffixStr);
    }
    /**
     * Returns the Hash for a file. Can be MD5, SHA-1, SHA-256, SHA-512.
     * @param file the file to be hashed
     * @return a string in hexadecimal that represents the hashed bytes
     * @throws IOException
     * @throws NoSuchAlgorithmException
     */
    public static String hashFile(File file, String algorithm, String suffixStr)
            throws IOException, NoSuchAlgorithmException {
        MessageDigest sha256 = MessageDigest.getInstance(algorithm);
        StringBuffer sb = new StringBuffer();
        byte[] data = new byte[1024];
        int read = 0;

        try (FileInputStream fis = new FileInputStream(file)) {
            while ((read = fis.read(data)) != -1) {
                sha256.update(data, 0, read);
            }

            if (suffixStr != null) {
                sha256.update(suffixStr.getBytes());
            }

            byte[] hashBytes = sha256.digest();

            for (int i = 0; i < hashBytes.length; i++) {
                sb.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
        }

        return sb.toString();
    }
    public void loadFiles(String path) throws IOException {
        if (this.bulk == true) {
            File f = new File(path);
            if (f.isDirectory()) {
                this.loadFilesFromFolder(path);
            } else {
                this.loadFilesFromFileList(path);
            }
        } else {
            this.addFile(path);
        }
    }

    public void loadFilesFromFileList(String path) {
        if (path == null || path == "") {
            logError("[ERROR] No File List provided");
            return;
        }

        File filelist = new File(path);

        if (!filelist.exists()) {
            logError("[ERROR] File List: '" + path + "' does not exists!");
            return;
        }
        if (!filelist.isFile()) {
            logError("[ERROR] File List: '" + path + "' is not a file. "
                    + "Try adding the '-bulk' option if you wish to use a folder");
            return;
        }

        try {
            String mimeType = Files.probeContentType(filelist.toPath());

            if (!mimeType.contains("text/plain")) {
                logError("[Error] File List must be of MIME type 'text/plain', the file given is of type: '" + mimeType
                        + "'");
                return;
            }
        } catch (IOException e1) {
            logError("[Error] Unable to determine the MIME type of the file.");
            return;
        }

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(filelist));
            String line;
            while ((line = reader.readLine()) != null) {
                // skip empty lines and comments
                if (line.isEmpty() || line.startsWith("#")) {
                    continue;
                }

                File f = new File(line);

                if (f.exists()) {
                    files.add(f);
                } else {
                    logError("[WARNING] File does not exists: \"" + line + "\"");
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void loadFilesFromFolder(String path) {
        if (path == null || path == "") {
            logError("[ERROR] No Folder provided");
            return;
        }

        File folder = new File(path);

        if (!folder.exists()) {
            logError("[ERROR] Folder: '" + path + "' does not exists!");
            return;
        }
        if (!folder.isDirectory()) {
            logError("[ERROR] Folder: '" + path + "' is not a folder and the 'bulk' option is present. "
                    + "Try removing the '-bulk' option if you wish to use "
                    + "a specific file, or change the option to point to a folder");
            return;
        }

        for (File f : folder.listFiles()) {
            if (f.isFile()) {
                if (f.getName().endsWith(fileExt)) {
                    log.fine("Adding file to specFiles: " + f.getAbsolutePath());
                    files.add(f);
                }
            } else if (f.isDirectory()) {
                log.fine("Adding folder: " + f.getAbsolutePath());
                loadFilesFromFolder(f.getAbsolutePath());
            }
        }
    }

    private void logError(String msg) {
        lastError = msg;
        errorLog.append(msg);
        log.severe(msg);
    }

    public void addFile(String path) throws IOException {
        if (path == null || path.equals("")) {
            logError("[ERROR] No File provided");
        }

        File file2 = new File(path);
        if (!file2.exists()) {
            logError("[ERROR] File: '" + path + "' does not exists!");
        }
        if (!file2.isFile()) {
            logError("[ERROR] File: '" + path + "' is not a file and the 'bulk' option was not set. "
                    + "Try setting the '-bulk' option if you wish to search "
                    + "through the folder, or change the option to point to a specific file");
        }

        log.fine("[FILE IO] Using File: " + file2.getCanonicalPath());
        files.add(file2);
    }

    /** Prints the file list with an optional character limit
     * 
     * @param characterLimit
     *            if less than 0 then it will have no limit,
     *            else the resulting string will be less than or equal to the characterLimit
     * @param absolutePath
     *            if TRUE then it will print the absolute path of the file,
     *            else it will print the filename
     * @return */
    public String printFileNames(int characterLimit, boolean absolutePath) {
        StringBuilder s = new StringBuilder();
        String tmp;

        s.append("[");
        for (int i = 0; i < files.size(); i++) {
            if (absolutePath) {
                tmp = files.get(i).getAbsolutePath();
            } else {
                tmp = files.get(i).getName();
            }
            if (s.length() + tmp.length() > characterLimit && characterLimit > 0) {
                break;
            }
            s.append(tmp).append(",");
        }
        s.append("]");
        return s.toString();
    }

    /**
     * Returns the number of files stored.
     * @return
     */
    public int size() {
        return files.size();
    }

    /**
     * Returns the file object at a specific index
     * @param index
     * @return
     */
    public File get(int index) {
        return files.get(index);
    }

    /**
     * Returns true if a stored file has the same filename, does not look at the path. 
     * @param filename
     * @return
     */
    public boolean contains(String filename) {
        return search(filename) != null;
    }

    /**
     * Returns the first stored file with the same filename, does not look at the path.
     * 
     * Returns NULL, if a file cannot be found.
     * @param filename
     * @return
     */
    public File search(String filename) {
        File found = null;

        for (File f : files) {
            if (f.getName().equals(filename)) {
                found = f;
                break;
            }
        }

        return found;
    }

    /**
     * Replaces the last occurrence of the oldExtension it finds with newExtension.
     * <br>
     * If the oldExtension is not found then the original filename is returned.
     * 
     * @param filename
     * @param oldExtension
     * @param newExtension
     * @return
     */
    public static String swapExtension(String filename, String oldExtension, String newExtension) {
        int lastIndex = filename.lastIndexOf(oldExtension);
        if (lastIndex < 0) return filename;
        String tail = filename.substring(lastIndex).replaceFirst(oldExtension, newExtension);
        return filename.substring(0, lastIndex) + tail;
    }
    /**
     * Finds the last occurrence of '.' and replaces everything after it (including the period) with newExtension.
     * <br>
     * If '.' is not found, then newExtension will be added to the filename.
     * 
     * @param filename
     * @param newExtension
     * @return
     */
    public static String swapExtension(String filename, String newExtension) {
        int lastIndex = filename.lastIndexOf(".");
        if (lastIndex < 0) return filename + newExtension;

        return filename.substring(0, lastIndex) + newExtension;
    }
}
