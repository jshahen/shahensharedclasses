package shahen.string;

import java.util.ArrayList;

public class ShahenStrings {
    public static String CSVEscapeString(String s) {
        if (s == null) return null;
        s = s.replace("\"", "\"\"");
        if (s.contains(",") || s.contains("\n") || s.contains("\"")) { return "\"" + s + "\""; }
        return s;
    }

    public static String ArrayJoin(String[] ss, String separator) {
        StringBuilder s = new StringBuilder();

        for (int i = 0; i < ss.length; i++) {
            if (i > 0) {
                s.append(separator);
            }
            s.append(ss[i]);
        }

        return s.toString();
    }

    /**
     * A native recreation of a string wrap, more logic should go into it. But it works for now.
     * @param s
     * @param maxWidth
     * @param newLineStr
     * @param breakLongWords NOT USED
     * @return
     */
    public static String wrap(String s, int maxWidth, String newLineStr, boolean breakLongWords) {
        if (s == null) return null;

        if (s.length() <= maxWidth) return s;

        boolean endsWithNewLine = s.endsWith("\n");

        int sb_len = s.length() + newLineStr.length() * (1 + (int) (s.length() / maxWidth));
        StringBuilder sb = new StringBuilder(sb_len);

        String[] lines = s.split("\\r?\\n|\\r");
        for (String t : lines) {
            while (t.length() > 0) {
                if (t.length() > maxWidth) {
                    sb.append(t.subSequence(0, maxWidth + 1)).append(newLineStr);
                    t = t.substring(maxWidth + 1);
                } else {
                    sb.append(t);
                    t = "";
                }
            }
            sb.append("\n");
        }

        if (endsWithNewLine) return sb.toString();
        else return sb.toString().substring(0, sb.length() - 1);
    }

    public static String repeat(String string, int maxw) {
        if (string == null) return null;
        if (string.length() == 0) return null;

        StringBuilder sb = new StringBuilder(maxw);

        while (sb.length() < maxw) {
            sb.append(string);
        }
        if (sb.length() == maxw) return sb.toString();
        return sb.toString().substring(0, maxw);
    }

    public static ArrayList<Boolean> booleanStringToArrayList(String string) {
        if (string == null) return null;

        ArrayList<Boolean> bools = new ArrayList<Boolean>();

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '0') {
                bools.add(false);
            } else if (string.charAt(i) == '1') {
                bools.add(true);
            } else {
                throw new NumberFormatException("Input string contains non-binary numbers: " + string);
            }
        }

        return bools;
    }
    public static Boolean[] booleanStringToArray(String boolStr) {
        if (boolStr == null) return null;

        Boolean[] bools = new Boolean[boolStr.length()];

        for (int i = 0; i < boolStr.length(); i++) {
            if (boolStr.charAt(i) == '0') {
                bools[i] = false;
            } else if (boolStr.charAt(i) == '1') {
                bools[i] = true;
            } else {
                throw new NumberFormatException("Input string contains non-binary numbers: " + boolStr);
            }
        }

        return bools;
    }

    /**
     * Returns a string for the format:<br/>
     * <li><b>Input:</b> [0,1,1,0,null,1,0]
     * <li><b>Output:</b> "0110X10"
     * @param inputs
     * @return
     */
    public static String toBooleanString(ArrayList<Boolean> inputs) {
        StringBuilder sb = new StringBuilder(inputs.size());

        for (Boolean b : inputs) {
            if (b == null) sb.append('X');
            else if (b == true) sb.append('1');
            else sb.append('0');
        }

        return sb.toString();
    }

    /**
     * Returns a string for the format:<br/>
     * <li><b>Input:</b> [0,1,1,0,1,1,0]
     * <li><b>Output:</b> "0110110"
     * @param inputs
     * @return
     */
    public static String toBooleanString(Boolean[] inputs) {
        if (inputs == null) return "null";
        StringBuilder sb = new StringBuilder(inputs.length);

        for (Boolean b : inputs) {
            if (b == null) sb.append('X');
            else if (b == true) sb.append('1');
            else sb.append('0');
        }

        return sb.toString();
    }
    public static String toBooleanString(boolean[] inputs) {
        StringBuilder sb = new StringBuilder(inputs.length);

        for (Boolean b : inputs) {
            if (b == null) sb.append('X');
            else if (b == true) sb.append('1');
            else sb.append('0');
        }

        return sb.toString();
    }
    public static String toBooleanStringCPLEXFormat(Boolean[] inputs) {
        StringBuilder sb = new StringBuilder(inputs.length);

        sb.append("[");
        boolean first = true;
        for (Boolean b : inputs) {
            if (first) {
                first = false;
            } else {
                sb.append(" ");
            }
            if (b == null) sb.append('X');
            if (b == true) sb.append('1');
            else sb.append('0');
        }
        sb.append("]");

        return sb.toString();
    }

    public static int msInDay = 86400000;
    public static int msInHour = 3600000;
    public static int msInMinute = 60000;
    public static int msInSecond = 1000;
    /**
     * Converts a duration in <b>milliseconds</b> into a string of the form:
     * <ul>
     * <li> 12 ms
     * <li> 1 min 40 sec
     * <li> 12 hr 2 min 0 sec
     * <li> 50 days 5 ms
     * </ul>
     * @param durationMilliseconds the duration in milliseconds
     * @return a string representation that is easy to read and compare
     */
    public static String prettyDuration(Long durationMilliseconds) {
        StringBuilder sb = new StringBuilder();

        // Days
        if (durationMilliseconds >= msInDay) {
            int numDays = (int) Math.floor(durationMilliseconds / msInDay);
            durationMilliseconds = durationMilliseconds - (long) (numDays * msInDay);
            sb.append(numDays);
            if (numDays > 1) {
                sb.append(" days");
            } else {
                sb.append(" day");
            }
        }

        // Hours
        if (durationMilliseconds >= msInHour) {
            int numHours = (int) Math.floor(durationMilliseconds / msInHour);
            durationMilliseconds = durationMilliseconds - (long) (numHours * msInHour);

            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(numHours).append(" hr");
        }

        // Minutes
        if (durationMilliseconds >= msInMinute) {
            int numMinutes = (int) Math.floor(durationMilliseconds / msInMinute);
            durationMilliseconds = durationMilliseconds - (long) (numMinutes * msInMinute);

            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(numMinutes).append(" min");
        }

        // Seconds
        if (durationMilliseconds >= msInSecond) {
            int numSeconds = (int) Math.floor(durationMilliseconds / msInSecond);
            durationMilliseconds = durationMilliseconds - (long) (numSeconds * msInSecond);

            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(numSeconds).append(" sec");
        }

        // Milliseconds
        if (durationMilliseconds >= 0) {
            if (sb.length() > 0) {
                sb.append(" ");
            }

            sb.append(durationMilliseconds).append(" ms");
        }

        return sb.toString();
    }

    /**
     * Zero pads a number
     * @param input_num
     * @param outputLength
     * @return
     */
    public static String zeroPad(int input_num, int outputLength) {
        String num = "" + input_num;

        if (num.length() >= outputLength) return num;

        return repeat("0", outputLength - num.length()) + num;
    }
}
