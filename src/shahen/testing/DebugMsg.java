package shahen.testing;

public class DebugMsg {
    public static void msg(Boolean display, String msg) {
        if (display) {
            System.out.println("[DEBUG] " + msg);
        }
    }

    public static void header(Boolean display) {
        if (display) {
            System.out.println("#################################################");
        }
    }
}
