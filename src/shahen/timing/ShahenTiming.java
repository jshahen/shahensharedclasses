package shahen.timing;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

import shahen.string.ShahenStrings;

public class ShahenTiming {

    public final static Logger logger = Logger.getLogger("mohawk");
    private Map<String, ShahenTimingEvent> timings = new Hashtable<String, ShahenTimingEvent>();
    private String lastFinishedTimer = null;
    /** When a timer is stopped or cancelled, then a message will be printed to the console (not the log!)
     * Use this as a progress indicator when a human is monitoring a long task. */
    public Boolean printOnStop = false;
    public DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    public String[] heading = new String[]{"Category", "Start Time", "Start Milliseconds", "Finish Time",
            "Finish Milliseconds", "Timer Key", "Duration (ms)", "Comment"};

    public Map<String, ShahenTimingEvent> getTimings() {
        return timings;
    }

    public void blankTimer(String key) {
        timings.put(key, new ShahenTimingEvent());
    }

    /**
     * Creates and starts a timer given by the 'key'.
     * @param key
     * @return the timing event
     */
    public ShahenTimingEvent startTimer(String key) {
        ShahenTimingEvent t = timings.get(key);
        if (t == null) {
            t = new ShahenTimingEvent();
        }
        t.setStartTimeNow();
        timings.put(key, t);

        return t;
    }

    /**
     * Creates and starts a timer given by the key: prefix + key.
     * @param prefix
     * @param key
     * @return the timing event
     */
    public ShahenTimingEvent startTimer(String prefix, String key) {
        return startTimer(prefix + key);
    }

    /**
     * Stops the timing event based on the timer key given.
     * @param key
     * @return TRUE if the timer key was found; FALSE otherwise
     */
    public Boolean stopTimer(String key) {
        ShahenTimingEvent t = timings.get(key);
        if (t != null) {
            t.setFinishTimeNow();
            timings.put(key, t);
            lastFinishedTimer = key;

            if (printOnStop) {
                System.out.println(
                        "[TIMER STOPPED @ " + dateFormat.format(new Date()) + "] " + key + ": " + t.duration() + " ms");
            }

            return true;
        }
        return false;
    }
    /**
     * Stops the timing event based on the timer key given.
     * @param key
     * @return TRUE if the timer key was found; FALSE otherwise
     */
    public Boolean stopTimer(String prefix, String key) {
        return stopTimer(prefix + key);
    }

    public String getLastFinishedTimer() {
        return lastFinishedTimer;
    }

    /** Used to indicate that the operation failed after a certain amount of time. The timing event's
     * {@link ShahenTimingEvent#failed} flag will be set to TRUE and the finish time will be set to NOW using \
     * {@link ShahenTimingEvent#setFinishTimeNow()}.<br>
     * <b>NOTE:</b> This only occurs if the timing event has not finished yet, if it was finished previously then this
     * function performs no actions.
     * 
     * @param key  the exact timer key to cancel
     * @param quiet do not print anything (even if {@link MohawkTiming.printOnStop} == true)
     * @return TRUE if the timer key was found; FALSE otherwise
     * */
    public Boolean cancelTimer(String key, boolean quiet) {
        ShahenTimingEvent t = timings.get(key);
        if (t != null) {
            if (!t.hasFinished()) {
                t.setFinishTimeNow();
                t.failed();
                timings.put(key, t);
                lastFinishedTimer = key;

                if (printOnStop && !quiet) {
                    System.out.println(key + ": " + t.duration() + " ms");
                }
            } else {
                if (printOnStop && !quiet) {
                    System.out.println("[Key was already stopped] " + key + ": " + t.duration() + " ms");
                }
            }

            return true;
        }
        return false;
    }

    public Boolean cancelTimer(String key) {
        return cancelTimer(key, false);
    }

    /** This function will find all timers with the prefix {@code timerPrefix} and cancel them. This function is to
     * be used in the case of a class crashes of being forced to exit without cleaning up its timers.
     * 
     * @param timerPrefix If NULL, then all timing keys are cancelled
     */
    public void cancelAll(String timerPrefix) {
        for (String key : timings.keySet()) {
            if (timerPrefix == null || key.startsWith(timerPrefix)) {
                cancelTimer(key, true);
            }
        }
    }

    public void removeTimer(String key) {
        timings.remove(key);
    }

    public Boolean writeOut(File results) throws IOException {
        return writeOut(results, false);
    }

    public Boolean writeOut(File results, Boolean forceOverwrite) throws IOException {
        Boolean writeHeader = forceOverwrite;
        if (!results.exists()) {
            if (!results.createNewFile()) {
                logger.warning("[TIMING] Unable to create the file: " + results.getAbsolutePath());
                return false;
            }
            writeHeader = true;
        }

        if (!results.isFile()) {
            logger.warning("[TIMING] Parameter pointing to something that is not a file: " + results.getAbsolutePath());
            return false;
        }

        FileWriter fw = new FileWriter(results, !forceOverwrite);

        if (writeHeader) {
            writeHeader(fw);
        }

        for (Map.Entry<String, ShahenTimingEvent> entry : timings.entrySet()) {
            if (writeOutSingle(fw, entry.getKey()) == false) {
                logger.warning("[TIMING] Unable to write the row: " + entry.toString());
                return false;
            }
        }
        fw.close();

        return true;
    }

    public Boolean writeOutLast(FileWriter fw) throws IOException {
        return writeOutSingle(fw, lastFinishedTimer);
    }

    public Boolean writeOutSingle(FileWriter fw, String key) throws IOException {
        ShahenTimingEvent value = timings.get(key);

        if (value == null) { return false; }

        try {
            fw.write(value.category + "," + value.startTime + "," + value.startTimeMilliSec + "," + value.finishTime
                    + "," + value.finishTimeMilliSec + "," + ShahenStrings.CSVEscapeString(key) + "," + value.duration()
                    + "," + value.comment + "\n");
        } catch (NullPointerException e) {
            System.out.println("[ERROR] NullPointer when trying to access: " + key);
            logger.severe("[ERROR] NullPointer when trying to access: " + key);
        }
        fw.flush(); // flushes just in-case this function is run once in a while!
        return true;
    }

    public void writeHeader(FileWriter fw) throws IOException {
        fw.write(ShahenStrings.ArrayJoin(heading, ",") + "\n");
    }

    @Override
    public String toString() {
        StringBuilder t = new StringBuilder();
        int i = 0;
        String s;
        for (Map.Entry<String, ShahenTimingEvent> entry : timings.entrySet()) {
            t.append(entry.getKey() + ": ");
            try {
                s = entry.getValue().toString();
            } catch (NullPointerException e) {
                s = "Not Finished";
            }
            t.append(s);

            i++;
            if (i != timings.size()) {
                t.append("; ");
            }
        }
        return t.toString();
    }

    public Long getTimerElapsedTime(String key) {
        if (timings.containsKey(key)) {
            ShahenTimingEvent te = timings.get(key);
            return te.duration();
        }

        return -1L;
    }

    /** Returns the Elapsed Time in Seconds (with decimal places)
     * 
     * @return */
    public Double getLastElapsedTimeSec() {
        ShahenTimingEvent te = timings.get(getLastFinishedTimer());

        return te.durationSec();
    }

    /** Return the Elapsed Time in Milliseconds*/
    public Long getLastElapsedTime() {
        ShahenTimingEvent te = timings.get(getLastFinishedTimer());

        return te.duration();
    }
    /** Returns a string of the last elapsed time using {@link ShahenStrings#prettyDuration(Long)} */
    public String getLastElapsedTimePretty() {
        return ShahenStrings.prettyDuration(getLastElapsedTime());
    }
}
